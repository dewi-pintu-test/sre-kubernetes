# Build image
docker build -t dewisiburian17/golang .

# Push container
docker push dewisiburian17/golang

# Create container
docker container create --name golang -p 8080:8080 dewisiburian17/golang

# See list of all container
docker container ls --all

# Start container
docker container start golang

# See list of all running container
docker container ls

# See container logs
docker container logs -f golang

# Stop container
docker container stop golang

# Remove container
docker container rm golang