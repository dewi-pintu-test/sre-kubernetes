# Build image
docker build -t dewisiburian17/nodejs .

# Push container
docker push dewisiburian17/nodejs

# Create container
docker container create --name nodejs dewisiburian17/nodejs

# See list of all container
docker container ls --all

# Start container
docker container start nodejs

# See container logs
docker container logs -f nodejs

# Stop container
docker container stop nodejs

# Remove container
docker container rm nodejs
